# Descomplicando o Gitlab

Treinamento ao vivo na Twitch - Descomplicando o Gitlab


### Day 1:
```bash
  - Entendenmos o que é o git
  - Entendemos o que é o GitLab
  - Como criar um Grupo no Gitlab
  - Como criar um Repositório Git
  - Aprendemos os coamndos básicos para manipulação de arquivos e diretórios e diretorio no Git
  - Como criar uma branch 
  - Como criar uma Merge Request
  - Como adicionar um membro no projeto
  - Como fazer o merge na Master/Main
  - Como associar um repo local com um repo remoto
  - Como importar um repo do GitHub para o Gitlab
  - Mudamos a branch padrão para Main
```